package com.tasj;

import com.tasj.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.tasj.pages.TodoMVC.*;

public class TodosE2ETest extends BaseTest {

    @Category(Smoke.class)
    @Test
    public void testTasksLifeCycle() {
//changes for job trigger
        givenAtAll();
        add("a");
        startEditing("a", "a edited").pressEnter();
        toggle("a edited");

        filterActive();
        assertItemsLeft(0);
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("a edited");

        filterAll();
        delete("a edited");
        assertNoTasks();
    }
}
